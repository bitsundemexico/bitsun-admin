/* eslint-env node */
'use strict';

module.exports = function ( /* environment, appConfig */) {
	return {
		sassOptions: {
			includePaths: [
        'node_modules/materialize-css/sass/components',
        'node_modules/themify-icons/',
			]
		}
	};
};
