import Ember from 'ember';
// import layout from '../templates/components/drop-menu';

export default Ember.Component.extend({
	layoutName: 'bitsun-admin/templates/components/drop-menu',
	tagName: 'li',
	tooltip: null,
	classNameBindings: ['active'],
  attributeBindings:['data-id'],
	active: false,
	didReceiveAttrs() {
		let actions = {};
		for (const key of Object.keys(this.get('fns'))) {
			actions[key] = this.get('fns')[key].bind(this._targetObject);
		}
		this.set('actions', actions);
	},
	didRender() {
		this._super(...arguments);
		// console.log(Ember.getOwner(this).lookup('controller:application').target);
    // if ( this.get('model.route') ) {
    this.set('active', Ember.getOwner(this).lookup('controller:application').target.currentRouteName === this.get('model.route'));
    // } else if (this.get('model.param.id')) {
      // this.set('active', Ember.getOwner(this).lookup('controller:application').target.currentRouteName.indexOf(this.get('model.param.id')) > -1);
    // }
		$(this.element).find('[data-toggle=tooltip]').tooltip();
	}
});
