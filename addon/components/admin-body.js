import Ember from 'ember';
import layout from '../templates/components/admin-body';

export default Ember.Component.extend({
  layout,
  tagName: 'section',
  classNames: ['admin-frame'],
  classNameBindings: ['expanded'],
  fullscreen: Ember.inject.service(),
  webapp: Ember.inject.service(),
  screen_mode: 'fullscreen',
  expanded: Ember.computed.alias('webapp.expanded'),
  expand_sidemenu: false,
  actions:{
    toggleMenu() {
      this.attrs.toggleMenu();
    }
  },
  didReceiveAttrs() {
		let actions = {};
		for (const key of Object.keys(this.get('fns') || {})) {
			actions[key] = this.get('fns')[key].bind(this._targetObject);
		}
		this.set('actions', actions);
	},
});
