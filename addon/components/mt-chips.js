/*global $*/
import Ember from 'ember';
import layout from '../templates/components/mt-chips';

export default Ember.Component.extend({
  layout,
  tagName: 'div',
  classNames: ['chips'],
  label: null,
  format(raw = '') {
    return raw.split(',').filter(t=>t).map(tag => {return {tag: tag}})
  },
  onchange(chips){
    let data = chips.map(c=>c.tag.trim()).join(',');
    this.set('source', data);
    data = this.format(data);
    let chip = $(this.element);
    chip.material_chip({data});
  },
  didInsertElement() {
    let data = this.format(this.get('source') || '');
    let chip = $(this.element);
    chip.material_chip({data});
    chip.on('chip.add chip.delete', ()=>{
      this.get('onchange').bind(this)(chip.material_chip('data'));
    });
  }
});
