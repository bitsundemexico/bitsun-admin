/*global $*/
import Ember from 'ember';
import layout from '../templates/components/mt-dropdown';

export default Ember.Component.extend({
  layout,
  tagName: 'ul',
  classNames: ['dropdown-content'],
  attributeBindings:[
    'data-induration:inDuration',
    'data-beloworigin:belowOrigin'
  ],
  didInsertElement() {
    $(this.element).dropdoswn({
     inDuration: 300,
     outDuration: 225,
     constrainWidth: false,
     hover: true,
     gutter: 0,
     belowOrigin: false,
     alignment: 'left',
     stopPropagation: false
   });
  }
});
