/*global $*/
import Ember from 'ember';
import layout from '../templates/components/select-single';

export default Ember.Component.extend({
  layout,
  tagName: 'div',
  value: null,
  actions: {
    update(value) {
      let val = value;
      if (this.get('useObj') || this.get('useIndexValue')) {
        val = this.get('source').objectAt(parseInt(value,10));
      } else {
        val = parseInt(value, 10);
      }
      this.set('value', val);
    }
  },
  sourceChange: Ember.observer('source', function() {
    Ember.run.scheduleOnce('afterRender', ()=>{
      let select = $(this.element).find('select');
      select.material_select();
    });
  }),
  didInsertElement() {
    let select = $(this.element).find('select');
    select.material_select();
  }
});
