/*global $*/
import Ember from 'ember';
import layout from '../templates/components/mt-modal';

export default Ember.Component.extend({
  layout,
  tagName: 'div',
  classNames: ['modal'],
  attributeBindings: ['id'],
  classNameBindings: ['bottom:bottom-sheet', 'fixed:modal-fixed-footer'],
  didInsertElement() {
    $(this.element).modal();
  }
});
