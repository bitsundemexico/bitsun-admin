import Ember from 'ember';
import sweeralert2 from 'ember-sweetalert2/mixins/sweetalert-mixin';

export default Ember.Mixin.create(sweeralert2, {
	fullscreen: Ember.inject.service(),
	webapp: Ember.inject.service(),
  // session: Ember.inject.service(),
  // sweetalert2: Ember.inject.service(),
	actions: {
    resolve(promise, errorArea, parser) {
      let sweetAlert = this.get('sweetAlert');
      sweetAlert({
        type: 'info',
				showCancelButton: false,
				showConfirmButton: false,
				allowOutsideClick: false,
				useRejections: false
      });
      sweetAlert.showLoading();
      return promise.then((res={})=>{
        return sweetAlert({
          title: res.title || 'Acción Completada',
          // text: res.body,
          type: 'success',
          timer: 2000,
          showCancelButton: false,
          confirmButtonText: 'Ok',
          confirmButtonColor: '#4CAF50',
          allowOutsideClick: false,
          useRejections: false
        }).then(()=>{
          return true;
        });
      }, (res)=>{
        return sweetAlert({
          title: res.title || 'Ha habido algunos errores',
          text: JSON.stringify(res.errors,2,2),
          type: 'error',
          showCancelButton: false,
          confirmButtonText: 'Notificar',
          confirmButtonColor: 'red',
          allowOutsideClick: false
        }).then(()=>{
          return false;
        });

      });
		},
		toggleFullScreen() {
			if (this.get('fullscreen.enabled')) {
				this.get('fullscreen.toggle')();
				this.set('screen_mode', this.get('fullscreen.fullscreen')() ? 'fullscreen_exit' : 'fullscreen');
				Ember.set(this, 'webapp.topmenu.fullscreen.icon.1', this.get('screen_mode'));
			}
		},
		toggleMenu() {
			// this.set('webapp.expanded', !this.get('webapp.toggleMenu'));
      this.get('webapp').toggleMenu();
		},
		invalidateSession() {
			Ember.$.post('/singout').then(() => {
				window.location.href = '/';
			});
	}}
});
