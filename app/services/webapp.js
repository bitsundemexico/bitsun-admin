import Ember from 'ember';
export default Ember.Service.extend({
  toggleMenu() {
    if(this.get('expandable')) {
      this.set('expanded', !this.get('expanded'));
    }
  },
	logo: '/img/bitsun_white.png',
	name: 'Bitsun',
	fullscreen: Ember.inject.service(),
	expanded: false,
  expandable: true,
	pages: [
  //   {
  //   name: 'Index',
  //   route: 'index',
	// 	icon: ['material-icons', 'fullscreen'],
	// 	action: 'toggleFullScreen',
	// 	tooltip: {
	// 		title: 'Toggle FullScreen',
	// 		placement: 'left'
	// 	}
	// }
],
	topmenu: {
		fullscreen: {
			icon: ['material-icons', 'fullscreen'],
			action: 'toggleFullScreen',
			tooltip: {
				title: 'Toggle FullScreen',
				placement: 'left'
			}
		}
  }
});
