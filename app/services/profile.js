import Ember from 'ember';
import ENV from '../config/environment';
const {inject: {service} } = Ember;

export default Ember.Service.extend({
  id: 'me',
  model: 'profile',
  store: service(),
  loaded: false,
  update () {
    return this.get('store').queryRecord(this.get('model'), {id: this.get('id')}).then( me => {
      this.set('me', me);
      return me;
    });
  },
  profile () {
    let me = this.get('store').queryRecord(this.get('model'), {id: this.get('id')});
    this.set('me', me);
    me.then( () => {
      this.set('loaded', true);
    });
    return me;
  }
});
