import Ember from 'ember';
import AjaxService from 'ember-ajax/services/ajax';
const {inject: {service} } = Ember;

export default AjaxService.extend({
  session: service(),
  headers: Ember.computed('session.data.authenticated', {
    get() {
      let headers = {};
      let token_type = this.get('session.data.authenticated.token_type');
			let access_token = this.get('session.data.authenticated.access_token');
      if (token_type) {
        headers['Authorization'] = `${token_type} ${access_token}`;
      }
      return headers;
    }
  })
});
